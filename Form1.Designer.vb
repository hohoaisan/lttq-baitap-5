﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class mainForm_51
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Label_FormTitle_51 = New System.Windows.Forms.Label()
        Me.Label_Name_51 = New System.Windows.Forms.Label()
        Me.Label_Class = New System.Windows.Forms.Label()
        Me.Label_GivenAsss = New System.Windows.Forms.Label()
        Me.Label_SelectedAss_51 = New System.Windows.Forms.Label()
        Me.Button_Exit_51 = New System.Windows.Forms.Button()
        Me.Button_Process_51 = New System.Windows.Forms.Button()
        Me.ComboBox_ListAss_51 = New System.Windows.Forms.ComboBox()
        Me.SuspendLayout()
        '
        'Label_FormTitle_51
        '
        Me.Label_FormTitle_51.AutoSize = True
        Me.Label_FormTitle_51.Font = New System.Drawing.Font("Segoe UI", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_FormTitle_51.Location = New System.Drawing.Point(85, 30)
        Me.Label_FormTitle_51.Name = "Label_FormTitle_51"
        Me.Label_FormTitle_51.Size = New System.Drawing.Size(480, 37)
        Me.Label_FormTitle_51.TabIndex = 0
        Me.Label_FormTitle_51.Text = "Bài tập Chương I, Chương II, Chương III"
        '
        'Label_Name_51
        '
        Me.Label_Name_51.AutoSize = True
        Me.Label_Name_51.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_Name_51.Location = New System.Drawing.Point(37, 104)
        Me.Label_Name_51.Name = "Label_Name_51"
        Me.Label_Name_51.Size = New System.Drawing.Size(164, 18)
        Me.Label_Name_51.TabIndex = 1
        Me.Label_Name_51.Text = "Họ và tên: Hồ Hoài Sản"
        '
        'Label_Class
        '
        Me.Label_Class.AutoSize = True
        Me.Label_Class.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_Class.Location = New System.Drawing.Point(37, 147)
        Me.Label_Class.Name = "Label_Class"
        Me.Label_Class.Size = New System.Drawing.Size(70, 18)
        Me.Label_Class.TabIndex = 2
        Me.Label_Class.Text = "Lớp 18T3"
        '
        'Label_GivenAsss
        '
        Me.Label_GivenAsss.AutoSize = True
        Me.Label_GivenAsss.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_GivenAsss.Location = New System.Drawing.Point(37, 192)
        Me.Label_GivenAsss.Name = "Label_GivenAsss"
        Me.Label_GivenAsss.Size = New System.Drawing.Size(155, 18)
        Me.Label_GivenAsss.TabIndex = 3
        Me.Label_GivenAsss.Text = "Các bài tập được nhận"
        '
        'Label_SelectedAss_51
        '
        Me.Label_SelectedAss_51.AutoSize = True
        Me.Label_SelectedAss_51.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_SelectedAss_51.Location = New System.Drawing.Point(37, 234)
        Me.Label_SelectedAss_51.Name = "Label_SelectedAss_51"
        Me.Label_SelectedAss_51.Size = New System.Drawing.Size(153, 18)
        Me.Label_SelectedAss_51.TabIndex = 4
        Me.Label_SelectedAss_51.Text = "Tên bài tập được nhận"
        '
        'Button_Exit_51
        '
        Me.Button_Exit_51.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button_Exit_51.Location = New System.Drawing.Point(465, 104)
        Me.Button_Exit_51.Name = "Button_Exit_51"
        Me.Button_Exit_51.Size = New System.Drawing.Size(187, 49)
        Me.Button_Exit_51.TabIndex = 5
        Me.Button_Exit_51.Text = "Thoát"
        Me.Button_Exit_51.UseVisualStyleBackColor = True
        '
        'Button_Process_51
        '
        Me.Button_Process_51.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button_Process_51.Location = New System.Drawing.Point(465, 175)
        Me.Button_Process_51.Name = "Button_Process_51"
        Me.Button_Process_51.Size = New System.Drawing.Size(187, 49)
        Me.Button_Process_51.TabIndex = 6
        Me.Button_Process_51.Text = "Thực hiện"
        Me.Button_Process_51.UseVisualStyleBackColor = True
        '
        'ComboBox_ListAss_51
        '
        Me.ComboBox_ListAss_51.FormattingEnabled = True
        Me.ComboBox_ListAss_51.Location = New System.Drawing.Point(208, 192)
        Me.ComboBox_ListAss_51.Name = "ComboBox_ListAss_51"
        Me.ComboBox_ListAss_51.Size = New System.Drawing.Size(182, 21)
        Me.ComboBox_ListAss_51.TabIndex = 7
        '
        'mainForm_51
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(684, 290)
        Me.Controls.Add(Me.ComboBox_ListAss_51)
        Me.Controls.Add(Me.Button_Process_51)
        Me.Controls.Add(Me.Button_Exit_51)
        Me.Controls.Add(Me.Label_SelectedAss_51)
        Me.Controls.Add(Me.Label_GivenAsss)
        Me.Controls.Add(Me.Label_Class)
        Me.Controls.Add(Me.Label_Name_51)
        Me.Controls.Add(Me.Label_FormTitle_51)
        Me.Name = "mainForm_51"
        Me.Text = "Sản_51"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label_FormTitle_51 As Label
    Friend WithEvents Label_Name_51 As Label
    Friend WithEvents Label_Class As Label
    Friend WithEvents Label_GivenAsss As Label
    Friend WithEvents Label_SelectedAss_51 As Label
    Friend WithEvents Button_Exit_51 As Button
    Friend WithEvents Button_Process_51 As Button
    Friend WithEvents ComboBox_ListAss_51 As ComboBox
End Class
