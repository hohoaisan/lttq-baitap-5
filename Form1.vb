﻿Public Class mainForm_51
    Private Sub mainForm_51_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'Lưu comboxBox list dưới dạng key-value (như XML hay JSON)
        'để tiện cho việc so sánh bằng key thay vì dùng value là tiêu đề
        Dim comboSource As New Dictionary(Of String, String)()
        comboSource.Add("Bài 4", "Diện tích hình tròn")
        comboSource.Add("Bài 11", "Tính tổng từ 1 đến n")
        comboSource.Add("Bài 30", "Số lẻ nhỏ nhất")
        comboSource.Add("Bài 39", "Ước số chung lớn nhất")
        comboSource.Add("Bài 45", "Chuyển bát phân sang thập phân")
        ComboBox_ListAss_51.DataSource = New BindingSource(comboSource, Nothing)
        ComboBox_ListAss_51.DisplayMember = "Key"
        ComboBox_ListAss_51.ValueMember = "Value"


    End Sub

    Private Sub Button_Exit_51_Click(sender As Object, e As EventArgs) Handles Button_Exit_51.Click
        Me.Close()
    End Sub

    Private Sub ComboBox_ListAss_51_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox_ListAss_51.SelectedIndexChanged
        'Lấy value của Selected item chính là tiêu đề của bài
        Label_SelectedAss_51.Text = DirectCast(ComboBox_ListAss_51.SelectedItem, KeyValuePair(Of String, String)).Value
    End Sub

    Private Sub Button_Process_51_Click(sender As Object, e As EventArgs) Handles Button_Process_51.Click
        Dim selectedForm_51 As String = DirectCast(ComboBox_ListAss_51.SelectedItem, KeyValuePair(Of String, String)).Key
        If selectedForm_51.Equals("Bài 4") Then
            Bai_4_Dientichhinhtron_51.Show()
            Return
        End If
        If selectedForm_51.Equals("Bài 11") Then
            Bai11_TinhTongTu1denN_51.Show()
            Return
        End If
        If selectedForm_51.Equals("Bài 30") Then
            Bai30_SoLeNhoNhat_51.Show()
            Return
        End If
        If selectedForm_51.Equals("Bài 39") Then
            Bai39_UCLNEclit_51.Show()
            Return
        End If
        If selectedForm_51.Equals("Bài 45") Then
            Bai45_BatPhanSangThapPhan_51.Show()
            Return
        End If
    End Sub
End Class
