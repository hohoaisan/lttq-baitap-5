﻿Public Class Bai30_SoLeNhoNhat_51
    Private numbers_51 As ArrayList = New ArrayList
    Function FindLowestOdd_51() As Integer
        'return 0: không có số lẻ nào trong dãy số
        'return x: có số lẻ nhỏ nhất
        Dim lowestodd As Integer = 0
        For Each value_51 As Integer In numbers_51
            If value_51 Mod 2 <> 0 Then
                If lowestodd <> 0 Then
                    If value_51 < lowestodd Then
                        lowestodd = value_51
                    End If
                Else
                    lowestodd = value_51
                End If
            End If
        Next
        Return lowestodd

    End Function

    Private Sub Bai30_SoLeNhoNhat_51_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        TextBox_Numbers_51.ReadOnly = True
        TextBox_Numbers_51.Text = ""
        numbers_51.Clear()

    End Sub

    Private Sub Button_ClearNumbers_51_Click(sender As Object, e As EventArgs) Handles Button_ClearNumbers_51.Click
        numbers_51.Clear()
        TextBox_Numbers_51.Text = ""
    End Sub

    Private Sub ButtonAddNumber_51_Click(sender As Object, e As EventArgs) Handles ButtonAddNumber_51.Click
        Try
            Dim tmp_51 As Integer = Integer.Parse(TextBox_AddNumber_51.Text)
            numbers_51.Add(tmp_51)
            TextBox_Numbers_51.Text = TextBox_Numbers_51.Text & " " & tmp_51
        Catch ex As Exception
            MsgBox("Dữ liệu nhập vào phải là số nguyên, " & ex.Message,, "Thông báo_51")
        End Try
    End Sub

    Private Sub Button_FindLowestOdd_51_Click(sender As Object, e As EventArgs) Handles Button_FindLowestOdd_51.Click
        Dim lowestOdd_51 As Integer = FindLowestOdd_51()
        If lowestOdd_51 = 0 Then
            MsgBox("Không có số lẻ nào trong dãy số",, "Thông báo_51")
        Else
            MsgBox("Số lẻ nhỏ nhất trong dãy số là " & lowestOdd_51,, "Thông báo_51")
        End If

    End Sub
End Class