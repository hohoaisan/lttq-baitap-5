﻿Public Class Bai45_BatPhanSangThapPhan_51
    Function toBinary_51(ByVal octal_51 As Integer) As String
        Dim result_51 As String = ""
        Do
            Dim digit_51 = octal_51 Mod 10
            If (digit_51 > 7) Then
                Throw New Exception("Số bát phân không có chữ số vượt quá 8")
            End If
            Dim part_51 As String = ""
            Do
                part_51 = digit_51 Mod 2 & part_51
                digit_51 = digit_51 \ 2
            Loop Until digit_51 = 0
            result_51 = Integer.Parse(part_51).ToString("000") & result_51
            octal_51 = octal_51 \ 10
        Loop Until octal_51 = 0
        Return result_51
    End Function
    Function toDecimal_51(ByVal binary_51 As String) As Integer
        Dim result As Integer = 0
        Dim len As Integer = binary_51.Length - 1
        While len >= 0
            result = result + Integer.Parse(binary_51(-len + binary_51.Length - 1)) * Math.Pow(2, len)
            len -= 1
        End While
        Return result
    End Function
    Private Sub Button_Convert_51_Click(sender As Object, e As EventArgs) Handles Button_Convert_51.Click
        Try
            Dim convertResult = toDecimal_51(toBinary_51(TextBox_Input_51.Text))
            MsgBox("Số thập phân chuyển từ hệ bát phân là " & convertResult, , "Chuyển đổi _51")
        Catch ex As Exception
            MsgBox(ex.Message, , "Chuyển đổi _51")
        End Try
    End Sub
End Class
