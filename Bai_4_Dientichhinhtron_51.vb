﻿Public Class Bai_4_Dientichhinhtron_51
    Dim area_51 As Double
    Private Sub getArea_51(ByVal radius_51 As Double, ByRef area As Double)
        If radius_51 < 0 Then
            Throw New Exception("Bán kính phải là số dương")
        End If
        area = radius_51 * radius_51 * Math.PI
    End Sub


    Private Sub Bai_4_Dientichhinhtron_51_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Label_Area_Result_51.Text = ""

    End Sub

    Private Sub Button_CalcArea_51_Click(sender As Object, e As EventArgs) Handles Button_CalcArea_51.Click
        Try
            Dim r As Double = Double.Parse(TextBox_Radius_51.Text)
            Call getArea_51(r, area_51)
            Label_Area_Result_51.Text = "Diện tích là " & area_51
        Catch ex As Exception
            MsgBox("Dữ liệu nhập vào không đúng" & vbNewLine & ex.Message,, "Thông báo")
        End Try

    End Sub
End Class