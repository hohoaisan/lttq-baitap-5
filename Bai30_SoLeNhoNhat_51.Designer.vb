﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Bai30_SoLeNhoNhat_51
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label_Numbers_51 = New System.Windows.Forms.Label()
        Me.TextBox_Numbers_51 = New System.Windows.Forms.TextBox()
        Me.Button_ClearNumbers_51 = New System.Windows.Forms.Button()
        Me.Button_FindLowestOdd_51 = New System.Windows.Forms.Button()
        Me.Label_AddNumber_51 = New System.Windows.Forms.Label()
        Me.ButtonAddNumber_51 = New System.Windows.Forms.Button()
        Me.TextBox_AddNumber_51 = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'Label_Numbers_51
        '
        Me.Label_Numbers_51.AutoSize = True
        Me.Label_Numbers_51.Location = New System.Drawing.Point(13, 24)
        Me.Label_Numbers_51.Name = "Label_Numbers_51"
        Me.Label_Numbers_51.Size = New System.Drawing.Size(40, 13)
        Me.Label_Numbers_51.TabIndex = 0
        Me.Label_Numbers_51.Text = "Dãy số"
        '
        'TextBox_Numbers_51
        '
        Me.TextBox_Numbers_51.Location = New System.Drawing.Point(108, 21)
        Me.TextBox_Numbers_51.Name = "TextBox_Numbers_51"
        Me.TextBox_Numbers_51.Size = New System.Drawing.Size(226, 20)
        Me.TextBox_Numbers_51.TabIndex = 1
        '
        'Button_ClearNumbers_51
        '
        Me.Button_ClearNumbers_51.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button_ClearNumbers_51.Location = New System.Drawing.Point(143, 131)
        Me.Button_ClearNumbers_51.Name = "Button_ClearNumbers_51"
        Me.Button_ClearNumbers_51.Size = New System.Drawing.Size(75, 23)
        Me.Button_ClearNumbers_51.TabIndex = 2
        Me.Button_ClearNumbers_51.Text = "Xoá hết"
        Me.Button_ClearNumbers_51.UseVisualStyleBackColor = True
        '
        'Button_FindLowestOdd_51
        '
        Me.Button_FindLowestOdd_51.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button_FindLowestOdd_51.Location = New System.Drawing.Point(223, 131)
        Me.Button_FindLowestOdd_51.Name = "Button_FindLowestOdd_51"
        Me.Button_FindLowestOdd_51.Size = New System.Drawing.Size(111, 23)
        Me.Button_FindLowestOdd_51.TabIndex = 3
        Me.Button_FindLowestOdd_51.Text = "Tìm số lẻ nhỏ nhất"
        Me.Button_FindLowestOdd_51.UseVisualStyleBackColor = True
        '
        'Label_AddNumber_51
        '
        Me.Label_AddNumber_51.AutoSize = True
        Me.Label_AddNumber_51.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.Label_AddNumber_51.Location = New System.Drawing.Point(13, 70)
        Me.Label_AddNumber_51.Name = "Label_AddNumber_51"
        Me.Label_AddNumber_51.Size = New System.Drawing.Size(89, 13)
        Me.Label_AddNumber_51.TabIndex = 4
        Me.Label_AddNumber_51.Text = "Thêm vào dãy số"
        '
        'ButtonAddNumber_51
        '
        Me.ButtonAddNumber_51.Location = New System.Drawing.Point(224, 66)
        Me.ButtonAddNumber_51.Name = "ButtonAddNumber_51"
        Me.ButtonAddNumber_51.Size = New System.Drawing.Size(110, 23)
        Me.ButtonAddNumber_51.TabIndex = 5
        Me.ButtonAddNumber_51.Text = "Thêm"
        Me.ButtonAddNumber_51.UseVisualStyleBackColor = True
        '
        'TextBox_AddNumber_51
        '
        Me.TextBox_AddNumber_51.Location = New System.Drawing.Point(108, 67)
        Me.TextBox_AddNumber_51.Name = "TextBox_AddNumber_51"
        Me.TextBox_AddNumber_51.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_AddNumber_51.TabIndex = 6
        '
        'Bai30_SoLeNhoNhat_51
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(339, 159)
        Me.Controls.Add(Me.TextBox_AddNumber_51)
        Me.Controls.Add(Me.ButtonAddNumber_51)
        Me.Controls.Add(Me.Label_AddNumber_51)
        Me.Controls.Add(Me.Button_FindLowestOdd_51)
        Me.Controls.Add(Me.Button_ClearNumbers_51)
        Me.Controls.Add(Me.TextBox_Numbers_51)
        Me.Controls.Add(Me.Label_Numbers_51)
        Me.Name = "Bai30_SoLeNhoNhat_51"
        Me.Text = "Sản_51_Bài 30"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label_Numbers_51 As Label
    Friend WithEvents TextBox_Numbers_51 As TextBox
    Friend WithEvents Button_ClearNumbers_51 As Button
    Friend WithEvents Button_FindLowestOdd_51 As Button
    Friend WithEvents Label_AddNumber_51 As Label
    Friend WithEvents ButtonAddNumber_51 As Button
    Friend WithEvents TextBox_AddNumber_51 As TextBox
End Class
