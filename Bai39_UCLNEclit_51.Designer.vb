﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Bai39_UCLNEclit_51
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TextBox_1stNumber_51 = New System.Windows.Forms.TextBox()
        Me.TextBox_2ndNumber_51 = New System.Windows.Forms.TextBox()
        Me.TextBox_3rdNumber_51 = New System.Windows.Forms.TextBox()
        Me.Label_NumbersInput_51 = New System.Windows.Forms.Label()
        Me.Button_CalcUCLN_51 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'TextBox_1stNumber_51
        '
        Me.TextBox_1stNumber_51.Location = New System.Drawing.Point(30, 47)
        Me.TextBox_1stNumber_51.Name = "TextBox_1stNumber_51"
        Me.TextBox_1stNumber_51.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_1stNumber_51.TabIndex = 0
        '
        'TextBox_2ndNumber_51
        '
        Me.TextBox_2ndNumber_51.Location = New System.Drawing.Point(139, 47)
        Me.TextBox_2ndNumber_51.Name = "TextBox_2ndNumber_51"
        Me.TextBox_2ndNumber_51.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_2ndNumber_51.TabIndex = 1
        '
        'TextBox_3rdNumber_51
        '
        Me.TextBox_3rdNumber_51.Location = New System.Drawing.Point(245, 47)
        Me.TextBox_3rdNumber_51.Name = "TextBox_3rdNumber_51"
        Me.TextBox_3rdNumber_51.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_3rdNumber_51.TabIndex = 2
        '
        'Label_NumbersInput_51
        '
        Me.Label_NumbersInput_51.AutoSize = True
        Me.Label_NumbersInput_51.Location = New System.Drawing.Point(147, 18)
        Me.Label_NumbersInput_51.Name = "Label_NumbersInput_51"
        Me.Label_NumbersInput_51.Size = New System.Drawing.Size(77, 13)
        Me.Label_NumbersInput_51.TabIndex = 3
        Me.Label_NumbersInput_51.Text = "Nhập vào 3 số"
        '
        'Button_CalcUCLN_51
        '
        Me.Button_CalcUCLN_51.Location = New System.Drawing.Point(206, 86)
        Me.Button_CalcUCLN_51.Name = "Button_CalcUCLN_51"
        Me.Button_CalcUCLN_51.Size = New System.Drawing.Size(144, 23)
        Me.Button_CalcUCLN_51.TabIndex = 4
        Me.Button_CalcUCLN_51.Text = "Tìm ước số chung lớn nhất"
        Me.Button_CalcUCLN_51.UseVisualStyleBackColor = True
        '
        'Bai39_UCLNEclit_51
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(357, 117)
        Me.Controls.Add(Me.Button_CalcUCLN_51)
        Me.Controls.Add(Me.Label_NumbersInput_51)
        Me.Controls.Add(Me.TextBox_3rdNumber_51)
        Me.Controls.Add(Me.TextBox_2ndNumber_51)
        Me.Controls.Add(Me.TextBox_1stNumber_51)
        Me.Name = "Bai39_UCLNEclit_51"
        Me.Text = "Sản_51_Bài 30"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TextBox_1stNumber_51 As TextBox
    Friend WithEvents TextBox_2ndNumber_51 As TextBox
    Friend WithEvents TextBox_3rdNumber_51 As TextBox
    Friend WithEvents Label_NumbersInput_51 As Label
    Friend WithEvents Button_CalcUCLN_51 As Button
End Class
