﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Bai11_TinhTongTu1denN_51
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label_SumResult = New System.Windows.Forms.Label()
        Me.Label_TypeN_51 = New System.Windows.Forms.Label()
        Me.Button_Sum_51 = New System.Windows.Forms.Button()
        Me.TextBox_TypeN = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'Label_SumResult
        '
        Me.Label_SumResult.AutoSize = True
        Me.Label_SumResult.Location = New System.Drawing.Point(35, 71)
        Me.Label_SumResult.Name = "Label_SumResult"
        Me.Label_SumResult.Size = New System.Drawing.Size(0, 13)
        Me.Label_SumResult.TabIndex = 7
        '
        'Label_TypeN_51
        '
        Me.Label_TypeN_51.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label_TypeN_51.AutoSize = True
        Me.Label_TypeN_51.Location = New System.Drawing.Point(35, 34)
        Me.Label_TypeN_51.Name = "Label_TypeN_51"
        Me.Label_TypeN_51.Size = New System.Drawing.Size(42, 13)
        Me.Label_TypeN_51.TabIndex = 6
        Me.Label_TypeN_51.Text = "Nhập n"
        '
        'Button_Sum_51
        '
        Me.Button_Sum_51.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button_Sum_51.Location = New System.Drawing.Point(169, 110)
        Me.Button_Sum_51.Name = "Button_Sum_51"
        Me.Button_Sum_51.Size = New System.Drawing.Size(107, 23)
        Me.Button_Sum_51.TabIndex = 5
        Me.Button_Sum_51.Text = "Tính tổng"
        Me.Button_Sum_51.UseVisualStyleBackColor = True
        '
        'TextBox_TypeN
        '
        Me.TextBox_TypeN.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBox_TypeN.Location = New System.Drawing.Point(98, 31)
        Me.TextBox_TypeN.Name = "TextBox_TypeN"
        Me.TextBox_TypeN.Size = New System.Drawing.Size(149, 20)
        Me.TextBox_TypeN.TabIndex = 4
        '
        'Bai11_TinhTongTu1denN_51
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(283, 138)
        Me.Controls.Add(Me.Label_SumResult)
        Me.Controls.Add(Me.Label_TypeN_51)
        Me.Controls.Add(Me.Button_Sum_51)
        Me.Controls.Add(Me.TextBox_TypeN)
        Me.Name = "Bai11_TinhTongTu1denN_51"
        Me.Text = "Sản_51_Bài 11"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label_SumResult As Label
    Friend WithEvents Label_TypeN_51 As Label
    Friend WithEvents Button_Sum_51 As Button
    Friend WithEvents TextBox_TypeN As TextBox
End Class
