﻿Public Class Bai11_TinhTongTu1denN_51
    Private Function SumN_51(ByVal n_51 As Integer)
        If n_51 >= 1 Then
            Return n_51 + SumN_51(n_51 - 1)
        End If
        Return 0

    End Function


    Private Sub Button_Sum_51_Click(sender As Object, e As EventArgs) Handles Button_Sum_51.Click
        Try
            Dim n_51 As Integer = Integer.Parse(TextBox_TypeN.Text)
            If n_51 < 1 Then
                Throw New Exception("Số nhập vào phải lớn hơn 0")
            End If
            Label_SumResult.Text = "Tổng S=1+2+3+...+" & n_51 & " là " & SumN_51(n_51)
        Catch ex As Exception
            MsgBox("Dữ liệu nhập vào phải là số hợp lệ" & vbNewLine & ex.Message,, "Thông báo")
        End Try



    End Sub

End Class