﻿Public Class Bai39_UCLNEclit_51
    Function UCLNEuclid(ByVal a_51 As Integer, ByVal b_51 As Integer) As Integer
        If b_51 = 0 Then Return a_51
        If a_51 Mod b_51 = 0 Then
            Return b_51
        Else Return UCLNEuclid(b_51, a_51 Mod b_51)
        End If
    End Function

    Private Sub Button_CalcUCLN_51_Click(sender As Object, e As EventArgs) Handles Button_CalcUCLN_51.Click
        Dim a_51 As Integer
        Dim b_51 As Integer
        Dim c_51 As Integer
        Dim result As Integer
        Try
            a_51 = Integer.Parse(TextBox_1stNumber_51.Text)
            b_51 = Integer.Parse(TextBox_2ndNumber_51.Text)
            c_51 = Integer.Parse(TextBox_3rdNumber_51.Text)
            result = UCLNEuclid(a_51, UCLNEuclid(b_51, c_51))
            MsgBox("Ước chung lớn nhất giữa 3 số là " & result,, "Kết quả_51")
        Catch ex As Exception
            MsgBox("Dữ liệu nhập vào phải là số nguyên",, "Thông báo_51")
        End Try
    End Sub
End Class