﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Bai_4_Dientichhinhtron_51
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.TextBox_Radius_51 = New System.Windows.Forms.TextBox()
        Me.Button_CalcArea_51 = New System.Windows.Forms.Button()
        Me.Label_Radius_51 = New System.Windows.Forms.Label()
        Me.Label_Area_Result_51 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'TextBox_Radius_51
        '
        Me.TextBox_Radius_51.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBox_Radius_51.Location = New System.Drawing.Point(149, 35)
        Me.TextBox_Radius_51.Name = "TextBox_Radius_51"
        Me.TextBox_Radius_51.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_Radius_51.TabIndex = 0
        '
        'Button_CalcArea_51
        '
        Me.Button_CalcArea_51.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button_CalcArea_51.Location = New System.Drawing.Point(171, 114)
        Me.Button_CalcArea_51.Name = "Button_CalcArea_51"
        Me.Button_CalcArea_51.Size = New System.Drawing.Size(107, 23)
        Me.Button_CalcArea_51.TabIndex = 1
        Me.Button_CalcArea_51.Text = "Tính diện tích"
        Me.Button_CalcArea_51.UseVisualStyleBackColor = True
        '
        'Label_Radius_51
        '
        Me.Label_Radius_51.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label_Radius_51.AutoSize = True
        Me.Label_Radius_51.Location = New System.Drawing.Point(37, 38)
        Me.Label_Radius_51.Name = "Label_Radius_51"
        Me.Label_Radius_51.Size = New System.Drawing.Size(79, 13)
        Me.Label_Radius_51.TabIndex = 2
        Me.Label_Radius_51.Text = "Nhập bán kính"
        '
        'Label_Area_Result_51
        '
        Me.Label_Area_Result_51.AutoSize = True
        Me.Label_Area_Result_51.Location = New System.Drawing.Point(37, 70)
        Me.Label_Area_Result_51.Name = "Label_Area_Result_51"
        Me.Label_Area_Result_51.Size = New System.Drawing.Size(0, 13)
        Me.Label_Area_Result_51.TabIndex = 3
        '
        'Bai_4_Dientichhinhtron_51
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(286, 143)
        Me.Controls.Add(Me.Label_Area_Result_51)
        Me.Controls.Add(Me.Label_Radius_51)
        Me.Controls.Add(Me.Button_CalcArea_51)
        Me.Controls.Add(Me.TextBox_Radius_51)
        Me.Name = "Bai_4_Dientichhinhtron_51"
        Me.Text = "Sản_51_Bài 4"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TextBox_Radius_51 As TextBox
    Friend WithEvents Button_CalcArea_51 As Button
    Friend WithEvents Label_Radius_51 As Label
    Friend WithEvents Label_Area_Result_51 As Label
End Class
